# Instructions

## Add Markup and Styles
1. Change the title text from 'Welcome to React' to 'Welcome to ~~React~~ Caesar Cipher' (keep the word React there but strike it out).
2. Put the title in a box and make some changes its appearance (style it however you like).
3. Below the `<header>` element, create a new box with three rows in it.
  - In the first row, put a text input with the label `CLEARTEXT` and a default value "HELLO WORLD"
  - In the second row, put a number input with a label `KEY` and a default value of 1.
  - In the third row, add a read-only text input with the label `CIPHERTEXT` and a value of "IFMMP XPSME"

4. Add some styles to the page and make it responsive. Do not spend too long on this, but add a few styles to make it look nice.

## Cipher Utils
In cipherUtils.js you will find two functions – `encrypt(input, key)` and `decrypt(input, key)` - that are yet to be implemented. You are going to first write tests, then implement these functions.

The input will be all upper case Latin letters - `ABCDEFGHIJKLMNOPQRSTUVWXYZ`

5. Write an appropriate number of unit tests in `cipherUtils.test.js` to test the correct functionality of these two functions. Each test should:
  - Fail initially
  - Be named appropriately
  - Test one thing, and one thing only
  - Be structured in an AAA (i.e. Arrange, Act, Assert) manner
6. Implement `encrypt(input, key)` and `decrypt(input, key)` so that the tests you have just created pass.

## Create the SimpleCaesarCipher component
7. Uncomment line 11 in `App.jsx`. Create a new Component called `SimpleCaesarCipher` and move the content you created in (3) into it.
8. Add state and/or props and functionality to your `SimpleCaesarCipher` component so that the `CIPHERTEXT` input is the encrypted version of the `CLEARTEXT` input, with the given `KEY`.  
  
  You can do this however you like. The only requirements are that you use `cipherUtils.encrypt()` and the `CIPHERTEXT` must change in real time, as the user changes the `CLEARTEXT` and `KEY` inputs.

  The only valid letters to be input are upper case Latin Letters - `ABCDEFGHIJKLMNOPQRSTUVWXYZ`. All other characters should be ignored.

## Add One More Feature
Add one small new feature of your choice. This is question is intentionally vague, and there is no right or wrong answer. It is your opportunity to show off your design and development abilities.

Some ideas:
- Add a 'Mode' radio button for selecting whether we want to encrypt or decrypt the text
- Make the background change colour each time a key is pressed
- Each time the cleartext changes, print out an ASCII-art representation of it to the console
