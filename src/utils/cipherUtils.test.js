import cipherUtils from "./cipherUtils";

describe("cipherUtils", () => {
  describe("encrypt", () => {
    it("Converts 'HELLO' to 'IFMMP' with a key of 1", () => {
      // arrange
      const expected = "IFMMP";

      // act
      const key = 1;
      const input = "HELLO";
      const actual = cipherUtils.encrypt(input, key);

      // assert
      expect(actual).toEqual(expected);
    });
  });
});
